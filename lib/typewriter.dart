/// typewriter provides a builder-pattern-friendly Typewriter widget for animating text.
library typewriter;

import 'package:flutter/widgets.dart';

/// A simple typewriter animation implementation.
///
/// Wraps a [text] along with a [controller] to provide a typing effect.
/// The caller specifies the duration and curve within the [controller],
/// and optionally passes in a [builder] for customizing the returned widget.
class Typewriter extends StatefulWidget {
  @required
  final String text;

  @required
  final AnimationController controller;

  final Function(String text) builder;

  const Typewriter(
    this.text, {
    Key key,
    this.controller,
    this.builder = _defaultBuilder,
  }) : super(key: key);

  static Widget _defaultBuilder(String text) {
    return Text(text);
  }

  @override
  _TypewriterState createState() => _TypewriterState(this.controller);
}

class _TypewriterState extends State<Typewriter>
    with SingleTickerProviderStateMixin {
  int end = 0;
  AnimationController controller;

  _TypewriterState([this.controller]);

  @override
  void initState() {
    super.initState();

    // fallback controller runs animation immediately after widget is built
    if (controller == null) {
      controller = AnimationController(
        duration: Duration(seconds: 1),
        vsync: this,
      );

      WidgetsBinding.instance.addPostFrameCallback((_) {
        controller.forward();
      });
    }

    controller.addListener(() {
      setState(() {
        end = (controller.value * widget.text.length) ~/ 1; // floor
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return widget.builder(
      widget.text.substring(0, this.end),
    );
  }
}
