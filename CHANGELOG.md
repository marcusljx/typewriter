## [0.0.1] - Prototype Release
* barebones prototype

## [0.0.2] - Update Docs
* Updated `README.md` and package description

## [0.0.3] - Adding default `AnimationController`
* Fixes #1: Not adding controller causes fatal NoSuchMethodError
