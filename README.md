# typewriter
A simple Flutter typewriter animation wrapper

# Installing
See [Installing Tab](https://pub.dev/packages/typewriter/install).

# Usage
This package exposes a single `Typewriter` Widget:
```
Typewriter("hello world!", 
  controller: _animationController,
);
```

You can create the [AnimationController](https://api.flutter.dev/flutter/animation/AnimationController-class.html) 
when you create your widget's state:
```dart
  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(
        seconds: 2,
      ),
    );
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
```

From there, you can trigger the `Typewriter`'s animation using the [AnimationController](https://api.flutter.dev/flutter/animation/AnimationController-class.html).
```dart
FlatButton(
  onPressed: () {
    _controller.forward();
  },
  child: Text("Tap to play animation"),
)
```

# Example
See [Example Tab](https://pub.dev/packages/typewriter/example) for a full example.
