import 'package:flutter/material.dart';
import 'package:typewriter/typewriter.dart';

void main() => runApp(Example1());

const TEXT = "Lorem ipsum dolor sit amet, "
    "consectetur adipiscing elit. "
    "Nunc ornare tellus arcu, "
    "in pharetra lorem volutpat et. "
    "Vestibulum ultricies eros sit amet ex suscipit facilisis. "
    "Cras fringilla diam eu erat euismod, "
    "nec ornare tortor pulvinar. "
    "Vivamus sed nisi ornare, "
    "iaculis tellus ut, "
    "ultrices ipsum. "
    "Pellentesque sagittis dui ac orci auctor, "
    "quis semper dui eleifend.";

class Example1 extends StatefulWidget {
  @override
  _Example1State createState() => _Example1State();
}

class _Example1State extends State<Example1>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(
        seconds: 2,
      ),
    );
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Container(
            color: Colors.blue,
            padding: EdgeInsets.all(10.0),
            constraints: BoxConstraints(maxWidth: 500),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Typewriter(
                  TEXT,
                  controller: _controller,
                ),
                FlatButton(
                  onPressed: () {
                    _controller.forward();
                  },
                  child: Text("Tap to play"),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
