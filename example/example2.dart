import 'package:flutter/material.dart';
import 'package:typewriter/typewriter.dart';

void main() => runApp(Example2());

const TEXT = "Lorem ipsum dolor sit amet, "
    "consectetur adipiscing elit. "
    "Nunc ornare tellus arcu, "
    "in pharetra lorem volutpat et. "
    "Vestibulum ultricies eros sit amet ex suscipit facilisis. "
    "Cras fringilla diam eu erat euismod, "
    "nec ornare tortor pulvinar. "
    "Vivamus sed nisi ornare, "
    "iaculis tellus ut, "
    "ultrices ipsum. "
    "Pellentesque sagittis dui ac orci auctor, "
    "quis semper dui eleifend.";

class Example2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Container(
            color: Colors.blue,
            padding: EdgeInsets.all(10.0),
            constraints: BoxConstraints(maxWidth: 500),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Typewriter(TEXT),
              ],
            ),

          ),
        ),
      ),
    );
  }
}
